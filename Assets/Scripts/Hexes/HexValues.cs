using System;
using UnityEngine;

namespace Hexes
{
    public class HexValues : MonoBehaviour
    {
        public static HexValues Instance;

        public bool CellOrientationVertical = true; //terrible name
        public float CellSideLength = 10.0f; //outer radius
        public float CellOuterRadius => CellSideLength;
        public float CellInnerRadius => CellSideLength * TriangleHeightConstant;

        //Hexagon is a collection of 6 equilateral triangles, the distance from the center of a
        //hexagon to a side is the same as the distance from a side of an equilateral triangle to a corner
        //multiply the side length by this constant to get that height
        //constant = sqroot(3)/2 ....
        //Assuming side length of 1, for any other side length multiply by derived constant (x)
        //1^2 = x^2 + (1/2)^2
        //1 = x^2 + 1/4
        //4 = 4x^2 + 1
        //3 = 4x^2
        //3/4 = x^2
        // sqroot(3)/2 = x
        public const float TriangleHeightConstant = 0.866025404f;
        
        //using singleton cus Unity sucks
        private void Awake()
        {
            if (Instance == this) return;
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }
    }
}