using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Hexes
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class HexCell : MonoBehaviour
    {
        [SerializeField] private TMP_Text coordinateText;

        private Mesh _hexMesh;

        private void Awake () {
            GetComponent<MeshFilter>().mesh = _hexMesh = new Mesh();
            _hexMesh.name = "Hex Mesh";
        }
        
        public void Init(int xCoord, int zCoord, float cellOuterRadius, float cellInnerRadius)
        {
            var pos = transform.position;
            coordinateText.transform.parent.transform.position = pos;
            Triangulate(cellOuterRadius, cellInnerRadius);

            coordinateText.text = $"{xCoord}, {zCoord}";
        }

        private void Triangulate(float cellOuterRadius, float cellInnerRadius)
        {
            var center = Vector3.zero;
            _hexMesh.Clear();
            //TODO make work for both orientations
            var cornerOffsets = new Vector3[]
            {
                new(0f, 0f, cellOuterRadius), //top corner
                new(cellInnerRadius, 0f, 0.5f * cellOuterRadius), //top right corner
                new(cellInnerRadius, 0f, 0.5f * -cellOuterRadius), //bottom right corner
                new(0f, 0f, -cellOuterRadius), //bottom corner
                new(-cellInnerRadius, 0f, 0.5f * -cellOuterRadius), //bottom left corner
                new(-cellInnerRadius, 0f, 0.5f * cellOuterRadius), //top left corner
            };
            
            //7 vertices
            var vertices = new Vector3[7];
            //6 triangles with 3 vertices each
            var triangles = new int[6 * 3];

            vertices[0] = center;
            var vertexCount = 1;
            var triangleCount = 0;
            
            for (var corner = 0; corner < 6; corner++)
            {
                vertices[vertexCount++] = center + cornerOffsets[corner];
                if (corner == 0) continue;
                //always has center point
                triangles[triangleCount * 3] = 0;
                triangles[triangleCount * 3 + 1] = vertexCount - 2;
                triangles[triangleCount++ * 3 + 2] = vertexCount - 1;
            }
            triangles[triangleCount * 3] = 0;
            triangles[triangleCount * 3 + 1] = vertexCount - 1;
            triangles[triangleCount * 3 + 2] = 1;

            _hexMesh.Clear();
            _hexMesh.vertices = vertices;
            _hexMesh.triangles = triangles;
            _hexMesh.RecalculateNormals();
        }
    }
}