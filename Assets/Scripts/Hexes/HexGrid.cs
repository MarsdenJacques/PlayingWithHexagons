using System.Collections.Generic;
using UnityEngine;

namespace Hexes
{
    public class HexGrid : MonoBehaviour
    {
        [SerializeField] private int boardSideLength = 6;
        [SerializeField] private HexCell hexCellPrefab;
        
        private HexCell[] _hexCells;
        private HexValues _hexValues;

        private void Start () {
            _hexCells = new HexCell[(int)Mathf.Pow(boardSideLength, 2)];
            _hexValues = HexValues.Instance;

            var columnLengths = GenerateColumnLengths();
            
            //TODO calculate coordinates
            //Use a containing square? hmm
            //Have a "square" of area (boardSideLength * 2 - 1)^2
            
            var cellCount = 0;
            for (var z = 0; z < boardSideLength; z++) {
                for (var x = 0; x < boardSideLength; x++) {
                    CreateCell(x, z, cellCount++);
                }
            }
        }

        private List<int> GenerateColumnLengths()
        {
            var columnLengths = new List<int>();
            var columnLength = boardSideLength;
            for (var columns = 1; columns <= boardSideLength * 2 - 1; columns++)
            {
                columnLengths.Add(columnLength);
                if (columns < boardSideLength) columnLength++;
                else columnLength--;
            }
            return columnLengths;
        }

        private void CreateCell (int x, int z, int i) {
            Vector3 position;
            //TODO make work for both orientations
            //each hex is offset by "inner diameter" horizontally
            position.x = 2.0f * _hexValues.CellInnerRadius;
            //every other row offset by an extra half of x
            position.x = (x + z % 2 * 0.5f) * position.x;
            position.y = 0f;
            //each hex center is 1.5 times the distance from the center to a corner away from the next vertically
            position.z = z * 1.5f * _hexValues.CellOuterRadius;

            var cell = _hexCells[i] = Instantiate(hexCellPrefab, position, Quaternion.identity, transform);
            cell.Init(x, z, _hexValues.CellOuterRadius, _hexValues.CellInnerRadius);
        }
    }
}
